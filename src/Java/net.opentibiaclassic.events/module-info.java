module net.opentibiaclassic.events {
    requires transitive org.json;
    requires transitive net.opentibiaclassic;
    requires transitive net.opentibiaclassic.jsonrpc;

    exports net.opentibiaclassic.events;
    exports net.opentibiaclassic.events.v770.game;
    exports net.opentibiaclassic.events.v770.game.client;
    exports net.opentibiaclassic.events.v770.game.server;
}