package net.opentibiaclassic.events.v770.game.server;

import org.json.JSONObject;

import net.opentibiaclassic.events.v770.game.ServerEvent;

import static net.opentibiaclassic.events.v770.game.ServerEvent.ServerEventType;

public class IdleWarningEvent extends ServerEvent {

    public IdleWarningEvent() {
        super(ServerEventType.IDLE_WARNING);
    }

    @Override
    public String toString() {
        return String.format(
            "IdleWarningEvent(UUID(%s),TimeUTC(%d),Source(%s),Type(%s))",
            this.uuid,
            this.timeUTC,
            this.source,
            this.type
        );
    }
}