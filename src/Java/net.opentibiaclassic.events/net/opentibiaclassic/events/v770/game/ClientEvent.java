package net.opentibiaclassic.events.v770.game;

import org.json.JSONObject;

import net.opentibiaclassic.events.TibiaEvent;

import net.opentibiaclassic.events.TibiaEvent.Source;

public abstract class ClientEvent extends TibiaEvent {
    public enum ClientEventType {
        ACTIVE ("Active");

        private final String name;

        ClientEventType(final String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return this.name;
        }
    }

    public ClientEvent(final ClientEventType type) {
        super(Source.CLIENT, type.toString());
    }

    @Override
    public abstract String toString();
}