package net.opentibiaclassic.events.v770.game.client;

import org.json.JSONObject;
import net.opentibiaclassic.events.v770.game.ClientEvent;

import static net.opentibiaclassic.events.v770.game.ClientEvent.ClientEventType;

public class ActiveEvent extends ClientEvent {

    private final int messageType;

    public ActiveEvent(final int messageType) {
        super(ClientEventType.ACTIVE);

        this.messageType = messageType;
    }

    @Override
    public JSONObject toJSONObject() {
        return super.toJSONObject()
            .put("message_type", this.messageType);
    }

    @Override
    public String toString() {
        return String.format(
            "ActiveEvent(UUID(%s),TimeUTC(%d),Source(%s),Type(%s),MessageType(%d))",
            this.uuid,
            this.timeUTC,
            this.source,
            this.type,
            this.messageType
        );
    }
}