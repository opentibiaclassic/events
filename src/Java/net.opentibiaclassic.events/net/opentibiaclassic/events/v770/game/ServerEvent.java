package net.opentibiaclassic.events.v770.game;

import org.json.JSONObject;

import net.opentibiaclassic.events.TibiaEvent;

import net.opentibiaclassic.events.TibiaEvent.Source;

public abstract class ServerEvent extends TibiaEvent {
    public enum ServerEventType {
        DAMAGE_RECEIVED ("DamageReceived"),
        IDLE_WARNING ("IdleWarning");

        private final String name;

        ServerEventType(final String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return this.name;
        }
    }

    public ServerEvent(final ServerEventType type) {
        super(Source.SERVER, type.toString());
    }

    @Override
    public abstract String toString();
}