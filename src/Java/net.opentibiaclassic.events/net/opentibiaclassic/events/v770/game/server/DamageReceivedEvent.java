package net.opentibiaclassic.events.v770.game.server;

import org.json.JSONObject;

import net.opentibiaclassic.events.v770.game.ServerEvent;

import static net.opentibiaclassic.events.v770.game.ServerEvent.ServerEventType;

public class DamageReceivedEvent extends ServerEvent {

    public final int damageAmount;

    public DamageReceivedEvent(final int damageAmount) {
        super(ServerEventType.DAMAGE_RECEIVED);

        this.damageAmount = damageAmount;
    }

    @Override
    public JSONObject toJSONObject() {
        return super.toJSONObject()
            .put("damage_amount", this.damageAmount);
    }

    @Override
    public String toString() {
        return String.format(
            "DamageReceivedEvent(UUID(%s),TimeUTC(%d),Source(%s),Type(%s),Amount(%d))",
            this.uuid,
            this.timeUTC,
            this.source,
            this.type,
            this.damageAmount
        );
    }
}