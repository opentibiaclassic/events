package net.opentibiaclassic.events;

import java.util.UUID;

import org.json.JSONObject;
import net.opentibiaclassic.jsonrpc.JSONObjectable;

import static net.opentibiaclassic.Util.*;

public abstract class TibiaEvent extends JSONObjectable {

    public enum Source {
        CLIENT, SERVER;
    }

    public final UUID uuid;
    public final long timeUTC;
    public final Source source;
    public final String type;

    public TibiaEvent(final Source source, final String type) {
        this.uuid = generateRandomTimeBasedUUID();
        this.timeUTC = System.currentTimeMillis();
        this.source = source;
        this.type = type;
    }

    @Override
    public JSONObject toJSONObject() {
        return (new JSONObject())
            .put("uuid", this.uuid.toString())
            .put("time_UTC", this.timeUTC)
            .put("source", this.source.toString())
            .put("type", this.type);
    }

    @Override
    public String toString() {
        return String.format(
            "TibiaEvent(UUID(%s),TimeUTC(%d),Source(%s),Type(%s))",
            this.uuid,
            this.timeUTC,
            this.source,
            this.type
        );
    }
}