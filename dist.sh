#!/bin/bash
. constants.env
jarfile=dist/net.opentibiaclassic.events_${release}.jar
set -x

rm -rf $jarfile
jar cf $jarfile -C build/Java/net.opentibiaclassic.events .